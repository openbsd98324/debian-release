# debian-release

 

````
1	Release history
1.1	Debian 1.1 (Buzz)
1.2	Debian 1.2 (Rex)
1.3	Debian 1.3 (Bo)
1.4	Debian 2.0 (Hamm)
1.5	Debian 2.1 (Slink)
1.6	Debian 2.2 (Potato)  
1.7	Debian 3.0 (Woody)
1.8	Debian 3.1 (Sarge)
1.9	Debian 4.0 (Etch)
1.10	Debian 5.0 (Lenny)
1.11	Debian 6.0 (Squeeze)
1.12	Debian 7 (Wheezy)
1.13	Debian 8 (Jessie)
1.14	Debian 9 (Stretch)
1.15	Debian 10 (Buster)
1.16	Debian 11 (Bullseye)  (stable,  2021)  
1.17	Debian 12 (Bookworm)  (testing, 2021) 
1.18	Debian 13 (Trixie)
````

# kernel

````
1	Release history
1.1	Debian 1.1 (Buzz)
1.2	Debian 1.2 (Rex)
1.3	Debian 1.3 (Bo)
1.4	Debian 2.0 (Hamm)
1.5	Debian 2.1 (Slink)
1.6	Debian 2.2 (Potato)  
1.7	Debian 3.0 (Woody)
1.8	Debian 3.1 (Sarge)
1.9	Debian 4.0 (Etch)
1.10	Debian 5.0 (Lenny)
1.11	Debian 6.0 (Squeeze)
1.12	Debian 7 (Wheezy)
1.13	Debian 8 (Jessie)  (kernel  vmlinuz-3.16.0-4-i386  for 586 and 686  )
1.14	Debian 9 (Stretch)
1.15	Debian 10 (Buster)
1.16	Debian 11 (Bullseye)  (stable,  2021)  
1.17	Debian 12 (Bookworm)  (testing, 2021) 
1.18	Debian 13 (Trixie)
````




# Change from 3.x to 4.x kernel 
````
Major upgrades include the Linux kernel going from version 3.16 to 4.9,
from 	Debian 8 (Jessie)
to 	Debian 9 (Stretch)
~# cat /etc/debian_version
9.6
~# uname -a
Linux xxxxxx 3.16.0-7-amd64 #1 SMP Debian 3.16.59-1 (2018-10-03) x86_64 GNU/Linux
````





# debian jessie /  Debian 8 (Jessie)

Debian 8

debian login: user

password: live



````
https://cdimage.debian.org/mirror/cdimage/archive/8.10.0/i386/iso-cd/debian-8.10.0-i386-CD-1.iso

linux-image-amd64 - Linux for 64-bit PCs (meta-package)
linux-image-3.16.0-7-amd64 - Linux 3.16 for 64-bit PCs
linux-image-3.2.0-6-amd64 - Linux 3.2 for 64-bit PCs
````


# debian stretch  /  Debian 9 (Stretch)

````
linux-headers-4.9.0-7-686 - Header files for Linux 4.9.0-7-686
linux-headers-4.9.0-7-686-pae - Header files for Linux 4.9.0-7-686-pae
linux-headers-4.9.0-7-rt-686-pae - Header files for Linux 4.9.0-7-rt-686-pae
linux-image-4.9.0-7-686 - Linux 4.9 for older PCs
linux-image-4.9.0-7-686-dbg - Debug symbols for linux-image-4.9.0-7-686
linux-image-4.9.0-7-686-pae - Linux 4.9 for modern PCs
linux-image-4.9.0-7-686-pae-dbg - Debug symbols for linux-image-4.9.0-7-686-pae
linux-image-4.9.0-7-rt-686-pae - Linux 4.9 for modern PCs, PREEMPT_RT
linux-image-4.9.0-7-rt-686-pae-dbg - Debug symbols for linux-image-4.9.0-7-rt-686-pae
````

# debian stable  /  Debian 11 (Stable)

2021

Bullseye (11.1) is the current stable release of Debian, and has been since 2021-10-09.


https://cdimage.debian.org/mirror/cdimage/archive/11.0.0/amd64/iso-dvd/debian-11.0.0-amd64-DVD-1.iso
